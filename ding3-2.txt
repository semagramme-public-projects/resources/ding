001 Or   tu
         00:00:01.236 - 00:00:01.416
         (0.76)

002 R    non bah + [pf]
         00:00:02.175 - 00:00:02.876

003 Or   ah tu peux y aller maintenant ton tour il était dans 3 (temps)
         00:00:02.874 - 00:00:05.308

004 O    [dés]
         00:00:05.310 - 00:00:06.214
         (1.01)

005 Or   12
         00:00:07.221 - 00:00:07.711
         (0.86)

006 Or   j'ai rien en 12 moi
         00:00:08.566 - 00:00:09.662
         (2.05)

007 Or   (il) y en a qu'une au 12
         00:00:11.712 - 00:00:12.912

008 B    tu es toute seule ouais
         00:00:12.088 - 00:00:12.923
         (1.51)

009 Or   j'aimerais bien faire 7 pour une fois
         00:00:14.438 - 00:00:15.880
         (0.64)

010 R    en fait t'as (te-) t'étais contente parce que juste tu as fait un double 6 et qu'en général c'est cool dans les jeux + [rire]
         00:00:16.518 - 00:00:21.910

011 Or   ouais c'est ça
         00:00:21.712 - 00:00:22.718

012 R    [rire]
         00:00:21.915 - 00:00:23.219

013 Or   aucune non (n-)
         00:00:23.232 - 00:00:25.183
         (0.03)

014 Or   non mais je savais que j'avais 10 11
         00:00:25.208 - 00:00:26.757

015 Or   tu vois j'ai eu 8 9 10 11
         00:00:26.776 - 00:00:28.647
         (0.04)

016 Or   donc euh (i-)
         00:00:28.687 - 00:00:29.929

017 B    (xxx)
         00:00:29.938 - 00:00:31.648
         (0.02)

018 Or   bon bah à toi
         00:00:31.669 - 00:00:32.976

019 Or   (-tain) j'avais plein de trucs à faire
         00:00:32.991 - 00:00:34.991
         (0.02)

020 Or   tu as fait un 7 et c'est tout foiré
         00:00:35.015 - 00:00:37.193

021 R    eh bah raté
         00:00:35.395 - 00:00:35.932

022 R    ah oui c'est vrai
         00:00:37.157 - 00:00:38.221

023 W    yes
         00:00:37.177 - 00:00:38.225

024 R    mon fameux 7
         00:00:38.241 - 00:00:39.499

025 W    ça c'est bien
         00:00:38.426 - 00:00:39.636
         (1.63)

026 Or   il est vachement bien ce jeu
         00:00:41.265 - 00:00:42.749
         (0.75)

027 B    tac euh
         00:00:43.502 - 00:00:44.873

028 Or   j'ai encore acheté un jeu (0.1s) par ta faute
         00:00:44.744 - 00:00:47.050

029 B    c'est toi qui récupères ça
         00:00:44.873 - 00:00:46.413

030 W    euh ouais
         00:00:46.421 - 00:00:47.341
         (0.10)

031 Or   merci
         00:00:47.437 - 00:00:48.470
         (2.35)

032 W    merci
         00:00:50.816 - 00:00:51.526
         (3.23)

033 W    hum
         00:00:54.754 - 00:00:55.964
         (1.31)

034 W    je vais me faire une route
         00:00:57.270 - 00:00:58.593
         (9.96)

035 W    et c'est tout
         00:01:08.551 - 00:01:09.196
         (0.86)

036 O    [dés]
         00:01:10.055 - 00:01:14.168
         (1.11)

037 B    (8) (8)
         00:01:15.274 - 00:01:16.194

038 R    1 mouton
         00:01:15.984 - 00:01:16.823
         (0.39)

039 Or   moi j'ai 2 bois
         00:01:17.217 - 00:01:18.121
         (0.59)

040 W    ouais
         00:01:18.709 - 00:01:18.935
         (3.11)

041 R    ça va le bois là en face
         00:01:22.040 - 00:01:23.088
         (0.16)

042 W    ça va ça va
         00:01:23.249 - 00:01:24.782
         (5.37)

043 Or   si jamais quelqu'un a trop de pierres euh on peut négocier
         00:01:30.150 - 00:01:33.085

044 B    dépôt de pierres quelqu'un + [rire]
         00:01:32.253 - 00:01:34.399
         (0.43)

045 B    c'est triste mais quelqu'un a de la pierre ?
         00:01:34.829 - 00:01:36.538
         (0.57)

046 W    qui a de la pierre ?
         00:01:37.110 - 00:01:38.256

047 B    oui
         00:01:38.022 - 00:01:38.570

048 W    c'est vrai que le 6 il est pas tombé depuis longtemps là
         00:01:38.272 - 00:01:40.143

049 R    hum
         00:01:39.968 - 00:01:40.129
         (1.51)

050 W    ben
         00:01:41.653 - 00:01:42.362
         (0.09)

051 R    ah oui le maître de la pierre
         00:01:42.451 - 00:01:43.805

052 B    ou alors du bois
         00:01:43.439 - 00:01:44.810
         (0.03)

053 B    du blé contre du bois
         00:01:44.842 - 00:01:45.890
         (0.03)

054 B    peut-être
         00:01:45.921 - 00:01:46.728

055 W    euh
         00:01:46.698 - 00:01:47.214
         (0.02)

056 W    ouais je veux bien du blé
         00:01:47.238 - 00:01:48.303
         (0.44)

057 B    (du) coup 2
         00:01:48.746 - 00:01:49.391
         (0.54)

058 W    2 contre 2
         00:01:49.926 - 00:01:50.458
         (0.69)

059 W    2 contre 1
         00:01:51.152 - 00:01:51.668
         (0.38)

060 B    2 contre 2 + [rire]
         00:01:52.044 - 00:01:53.415

061 W    j'avoue 2 contre 1 c'est bien + [rire]
         00:01:52.045 - 00:01:53.416
         (1.65)

062 B    je vais pas faire
         00:01:55.071 - 00:01:56.248
         (1.32)

063 W    faut pas être trop rapide dans les négoces et heureusement
         00:01:57.564 - 00:01:59.290
         (5.16)

064 B    tac
         00:02:04.446 - 00:02:05.059
         (8.47)

065 B    encore du blé + [i]
         00:02:13.530 - 00:02:15.272
         (1.26)

066 B    même taux 2 contre 1
         00:02:16.534 - 00:02:17.647
         (0.44)

067 W    non pas forcément
         00:02:18.082 - 00:02:19.711
         (0.04)

068 Or   moi j'en ai
         00:02:19.755 - 00:02:20.707

069 B    quelqu'un
         00:02:20.162 - 00:02:20.775
         (0.04)

070 Or   tu le veux contre quoi ton blé ?
         00:02:20.815 - 00:02:21.976

071 B    du (0.1s) du bois
         00:02:21.984 - 00:02:23.403
         (0.88)

072 Or   hum
         00:02:24.282 - 00:02:24.895
         (0.48)

073 Or   je sais pas ce qui m'intéresse
         00:02:25.379 - 00:02:27.073
         (0.48)

074 Or   tu n'as pas du tout de pierres
         00:02:27.550 - 00:02:28.550
         (0.34)

075 B    (non)
         00:02:28.890 - 00:02:29.261
         (1.77)

076 O    (xxx)
         00:02:31.028 - 00:02:31.915
         (0.44)

077 B    tu as 2 bois contre du blé
         00:02:32.360 - 00:02:33.747

078 R    j'ai pas de bois
         00:02:33.395 - 00:02:34.282
         (1.01)

079 B    tant pis
         00:02:35.290 - 00:02:36.064
         (2.94)

080 O    [dés]
         00:02:39.003 - 00:02:40.051
         (0.38)

081 W    ah bah
         00:02:40.430 - 00:02:41.075

082 R    6
         00:02:40.921 - 00:02:41.792

083 W    le v(oi)là
         00:02:41.325 - 00:02:41.792
         (0.25)

084 R    bah voilà
         00:02:42.041 - 00:02:42.606

085 B    [rire]
         00:02:42.589 - 00:02:43.976
         (9.87)

086 R    quelqu'un a du bois ?
         00:02:53.849 - 00:02:54.978
         (0.60)

087 W    j'en ai
         00:02:55.582 - 00:02:55.937
         (0.52)

088 Or   moi aussi
         00:02:56.456 - 00:02:56.972
         (1.20)

089 W    tu n'as pas d'argile par hasard ?
         00:02:58.174 - 00:02:59.061
         (0.11)

090 R    [i] + non
         00:02:59.173 - 00:03:00.399
         (0.81)

091 Or   de la pierre
         00:03:01.213 - 00:03:01.810

092 W    mais par contre tu as
         00:03:01.584 - 00:03:02.245
         (0.29)

093 R    j'ai pas de la pierre non plus
         00:03:02.531 - 00:03:03.547
         (1.09)

094 W    tu as des moutons
         00:03:04.637 - 00:03:05.427
         (0.17)

095 R    ouais
         00:03:05.598 - 00:03:05.969
         (0.38)

096 W    1 bois contre 1 mouton
         00:03:06.352 - 00:03:07.110
         (0.43)

097 R    1 bois contre 1 mouton
         00:03:07.538 - 00:03:08.570
         (4.36)

098 R    [i]
         00:03:12.925 - 00:03:13.522
         (0.89)

099 R    tac tac tac
         00:03:14.417 - 00:03:15.046

100 R    (en) fait j'avais 1 argile c'est celui que j'ai récupéré tout à l'heure
         00:03:15.055 - 00:03:16.732

101 R    mais bon si je te le donnais euh c'était pas
         00:03:16.751 - 00:03:19.476
         (0.14)

102 W    c'est pas très rentable forcément ouais
         00:03:19.617 - 00:03:21.359
         (0.49)

103 R    hum
         00:03:21.851 - 00:03:24.109
         (0.43)

104 R    oui j'ai mis 1 de chaque euh pour la colonie
         00:03:24.536 - 00:03:26.585
         (2.87)

105 R    ma colonie elle est là
         00:03:29.451 - 00:03:31.016
         (1.01)

106 R    comme ça je peux troquer 3 contre 1 en plus maintenant
         00:03:32.028 - 00:03:34.528
         (1.24)

107 W    ça vaut le coup
         00:03:35.767 - 00:03:36.606
         (0.28)

108 R    euh
         00:03:36.888 - 00:03:37.871

109 R    et puis voilà
         00:03:37.887 - 00:03:39.145
         (1.11)

110 Or   (merci)
         00:03:40.250 - 00:03:41.072
         (0.28)

111 O    [dés]
         00:03:41.347 - 00:03:42.750
         (1.77)

112 W    6
         00:03:44.517 - 00:03:44.694

113 W    ah bah
         00:03:44.710 - 00:03:45.452

114 R    des cailloux
         00:03:44.919 - 00:03:45.580

115 W    le retour
         00:03:45.476 - 00:03:46.250

116 B    qui veut de la pierre ? + [rire]
         00:03:46.242 - 00:03:47.339

117 Or   qui veut pas du blé par hasard
         00:03:47.314 - 00:03:49.508

118 B    euh non mais je veux bien du bois
         00:03:49.507 - 00:03:51.314

119 W    donc là tu t'es retrouvé avec 10 pierres maintenant là
         00:03:50.370 - 00:03:52.095

120 Or   tu veux combien (d-) genre euh
         00:03:51.821 - 00:03:54.079
         (0.53)

121 B    1 de bois ce sera bien
         00:03:54.612 - 00:03:55.886
         (0.35)

122 Or   3 pierres contre 2 bois ça t'intéresse
         00:03:56.236 - 00:03:58.316
         (0.79)

123 B    tu as quoi en blé (s-) ok
         00:03:59.107 - 00:04:00.655
         (2.57)

124 B    (merci)
         00:04:03.224 - 00:04:03.982
         (1.25)

125 Or   du coup
         00:04:05.234 - 00:04:06.363
         (4.64)

126 Or   je vais faire une ville
         00:04:11.005 - 00:04:12.731
         (12.06)

127 Or   (j'ai pris le pont)
         00:04:24.788 - 00:04:25.852
         (0.31)

128 R    vous avez bien géré là parce que du coup vous avez bloqué tous les autres coins
         00:04:26.160 - 00:04:29.580

129 B    (il) y avait pas une route ici
         00:04:26.711 - 00:04:27.808

130 Or   ah faut que je fasse une euh ouais
         00:04:28.619 - 00:04:30.425
         (0.41)

131 Or   faut que je le relie avant de faire quelque chose là
         00:04:30.832 - 00:04:33.510

132 B    ouais
         00:04:31.760 - 00:04:32.308

133 W    bah
         00:04:32.844 - 00:04:33.167

134 W    au moins comme ça ça empêche le l'autre personne de s'installer
         00:04:33.507 - 00:04:36.088
         (0.52)

135 R    ouais ah ouais ouais non complètement
         00:04:36.607 - 00:04:38.026

136 B    [rire]
         00:04:36.607 - 00:04:38.026
         (2.85)

137 W    en fait c'est aussi que (0.1) ça coûtait cher de rajouter encore une route ici
         00:04:40.880 - 00:04:44.219
         (0.67)

138 Or   faudrait de l'argile
         00:04:44.889 - 00:04:46.147

139 R    ouais
         00:04:45.155 - 00:04:45.558

140 B    problème de budget
         00:04:45.767 - 00:04:46.719
         (2.60)

141 W    9
         00:04:49.315 - 00:04:50.557
         (0.26)

142 W    il se passe quoi à 9 ?
         00:04:50.815 - 00:04:52.202
         (0.16)

143 Or   moi j'aurais moi j'ai
         00:04:52.359 - 00:04:54.085

144 B    argile et mouton
         00:04:52.368 - 00:04:53.771

145 W    argile là-bas
         00:04:53.061 - 00:04:54.125
         (0.10)

146 Or   3 moutons
         00:04:54.226 - 00:04:55.291
         (1.63)

147 R    oh oui
         00:04:56.920 - 00:04:57.630
         (2.38)

148 W    quelqu'un a de l'argile ?
         00:05:00.006 - 00:05:01.103

149 Or   3 moutons
         00:05:00.481 - 00:05:01.320
         (3.31)

150 Or   merci
         00:05:04.634 - 00:05:05.311
         (1.03)

151 Or   (petit mouton) 
         00:05:06.345 - 00:05:07.039

152 W    est-ce que (0.1) 1 bois contre 1 argile ça passe ?
         00:05:07.000 - 00:05:09.629
         (3.80)

153 B    oui oui
         00:05:13.427 - 00:05:14.750
         (2.47)

154 W    merci bien
         00:05:17.222 - 00:05:18.029
         (1.52)

155 W    puis c'est tout en fait
         00:05:19.546 - 00:05:20.513
         (0.05)

156 B    pas faire 7 pas faire 7 pas faire 7
         00:05:20.563 - 00:05:22.612

157 O    [dés]
         00:05:22.593 - 00:05:24.045
         (1.56)

158 R    alors
         00:05:25.601 - 00:05:26.408
         (0.38)

159 R    1 mouton
         00:05:26.783 - 00:05:27.589
         (0.42)

160 Or   moi j'en ai 2 fois
         00:05:28.011 - 00:05:28.995

161 B    euh du bois pour vous
         00:05:28.414 - 00:05:29.753
         (0.40)

162 W    2 par là et 1 ici
         00:05:30.148 - 00:05:31.148
         (2.99)

163 W    merci
         00:05:34.140 - 00:05:34.914
         (2.63)

164 Or   qu'est-ce que tu disais ?
         00:05:37.542 - 00:05:38.542
         (1.28)

165 Or   en gros ?
         00:05:39.824 - 00:05:40.163
         (1.54)

166 B    que (0.1) pas faire 7 + [rire]
         00:05:41.705 - 00:05:42.850

167 Or   ah oui d'accord
         00:05:42.833 - 00:05:43.559

168 B    c'était ça ce que je voulais dire mais + [rire]
         00:05:43.156 - 00:05:44.478
         (0.89)

169 B    en gros je vais faire une ville déjà
         00:05:45.366 - 00:05:46.882
         (0.39)

170 W    (en)fin moi j'aimerais bien qu'il y ait un 7 là
         00:05:47.271 - 00:05:48.577
         (1.44)

171 W    (xxx) là où il est
         00:05:50.013 - 00:05:50.851
         (0.08)

172 Or   ouais c'est pas faux moi aussi j'aimerais bien un 7
         00:05:50.929 - 00:05:53.865

173 B    en fait je vais déjà faire une colonie
         00:05:53.578 - 00:05:55.643
         (2.57)

174 Or   comme ça ils ont le monopole de l'argile
         00:05:58.212 - 00:05:59.583
         (0.03)

175 Or   ça les intéresse vachement
         00:05:59.612 - 00:06:01.306

176 B    je fais une colonie ici
         00:06:01.301 - 00:06:02.510

177 W    ouais c'est ça
         00:06:02.392 - 00:06:02.876
         (0.02)

178 B    que je vais transformer en ville directement
         00:06:02.900 - 00:06:04.481
         (0.93)

179 W    ouf attends
         00:06:05.415 - 00:06:06.899
         (0.13)

180 W    1 2 3 4 5 6 7 8
         00:06:07.031 - 00:06:10.192
         (0.81)

181 W    1 2 3 4 5 6 7 8 9 10
         00:06:10.998 - 00:06:14.031

182 B    bah oui j'ai gagné en fait
         00:06:13.554 - 00:06:14.861

183 W    ah oui
         00:06:14.373 - 00:06:14.825

184 R    c'est plus la ville la plus
         00:06:14.460 - 00:06:15.686

185 W    [rire]
         00:06:15.285 - 00:06:16.349

186 B    [rire]
         00:06:15.285 - 00:06:16.349
         (0.34)

187 W    ok
         00:06:16.688 - 00:06:17.059
         (1.65)

188 W    en effet ça a été vite la fin
         00:06:18.712 - 00:06:19.792
         (0.03)

189 R    ouais
         00:06:19.820 - 00:06:20.320
         (1.30)

190 Or   pourquoi 8 ?
         00:06:21.620 - 00:06:22.411
         (0.52)

191 W    euh parce qu'en fait il l'a en calculant là
         00:06:22.936 - 00:06:24.727

192 Or   ah oui il a celle-là aussi
         00:06:23.675 - 00:06:24.787
         (0.21)

193 W    ouais
         00:06:24.993 - 00:06:25.170
         (1.87)

194 W    ah mais moi j'avais un (xxx) caché quand même euh
         00:06:27.041 - 00:06:29.057
         (1.48)

195 R    moi j'avais 2 chevaliers
         00:06:30.535 - 00:06:31.858
         (0.44)

196 R    on n'a pas les 3 je crois hein
         00:06:32.295 - 00:06:33.392

197 W    euh ouais
         00:06:33.411 - 00:06:33.814
         (0.02)

198 W    pour avoir le minimum aussi de ces 3 ouais
         00:06:33.838 - 00:06:35.532
         (2.62)

199 R    ah j'ai même pas
         00:06:38.153 - 00:06:39.895

200 R    j'ai même pas utilisé mon pouvoir
         00:06:39.912 - 00:06:41.379
         (1.04)

201 W    donc au final ouais tu es loin devant nous quand même je crois
         00:06:42.421 - 00:06:44.243
         (0.25)

202 R    hum
         00:06:44.495 - 00:06:44.721

203 W    quoique non toi c'est ça va 5
         00:06:44.501 - 00:06:46.323

204 Or   moi j'ai 5
         00:06:45.049 - 00:06:45.904

205 R    moi j'ai 3
         00:06:46.333 - 00:06:47.301
         (0.41)

206 W    moi j'ai 4
         00:06:47.706 - 00:06:48.706
         (0.39)

207 W    ouais ouais tu es quand même bien devant
         00:06:49.094 - 00:06:50.916
         (1.64)

208 R    pourquoi 4 ?
         00:06:52.555 - 00:06:53.281
         (0.06)

209 W    le (monop-)
         00:06:53.340 - 00:06:53.953

210 W    euh j'ai le point de vie (xxx)
         00:06:53.969 - 00:06:55.485

211 R    ah oui
         00:06:55.469 - 00:06:55.969
         (2.64)

212 W    donc je te disais donc le monopole de la (0.1) pierre ça a pu bien servir je pense d'une certaine façon c'est c'est pas mal ça
         00:06:58.608 - 00:07:03.850

213 B    après je me suis pas trop dit (xxx) c'est surtout que j'avais un de des ressources de chaque et j'arrivais à m'en tirer tu vois
         00:07:03.859 - 00:07:08.149
         (0.30)

214 B    je pense c'est l'argile et je suis tombé ça a souvent fait 9 euh
         00:07:08.445 - 00:07:11.380

215 W    ouais c'est vrai que
         00:07:08.475 - 00:07:09.087
         (0.22)

216 B    plus souvent que le 11 et je pense ça a joué quand même
         00:07:11.605 - 00:07:13.895

217 W    c'est vrai que ça arrive plus plus souvent
         00:07:13.463 - 00:07:15.125
         (0.59)

218 W    bah en fait c'est vrai que moi je m'étais dit allez faut que je fasse le le port rapidement
         00:07:15.718 - 00:07:18.395
         (0.03)

219 W    ici j'aurai plein de bois mais ça paye pas + [rire]
         00:07:18.429 - 00:07:21.251
         (0.30)

220 W    alors qu'ici c'est vrai que ce trajet-là était beaucoup plus malin
         00:07:21.549 - 00:07:24.743

221 R    hum
         00:07:24.713 - 00:07:25.019
         (1.64)

222 R    c'est vrai que moi j'ai regardé les chiffres surtout et puis surtout je me suis dit merde en gros l'argile faut
         00:07:26.659 - 00:07:31.062

223 W    bah c'est vrai que
         00:07:31.075 - 00:07:31.865

224 R    c'est vrai que (il) faut essayer d'avoir toutes les ressources
         00:07:31.181 - 00:07:33.439

225 W    bah en fait l'argile était très rare sur euh le
         00:07:32.204 - 00:07:34.994

226 W    là la répartition de l'argile était assez euh
         00:07:35.007 - 00:07:37.313
         (0.12)

227 R    bah en plus là les 2 ils avaient 11
         00:07:37.431 - 00:07:39.673

228 W    assez rare
         00:07:37.487 - 00:07:38.197

229 W    ouais c'est ça ouais
         00:07:39.689 - 00:07:40.560

230 W    c'est assez compliqué
         00:07:40.577 - 00:07:41.658
         (2.12)

231 W    eh bien merci pour la partie
         00:07:43.782 - 00:07:45.008
         (0.02)

232 Or   oui
         00:07:45.032 - 00:07:45.274
         (0.35)

233 Or   merci
         00:07:45.626 - 00:07:46.594


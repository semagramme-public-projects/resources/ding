#!/bin/bash

udpipe_bin="./udpipe"
udpipe_model="french-spoken-ud-2.5-191206.udpipe"


#######################################
# Check if udpipe program is available
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
check_for_udpipe()
{
    if ! command -v $udpipe_bin &> /dev/null
    then
	echo "Please install udpipe (https://ufal.mff.cuni.cz/udpipe) and add it to \$PATH before running this script,"
	echo "or download udpipe and french-spoken 2.5 model by running download-tools.bash."
	exit 1
    fi
}


#######################################
# Convert Elan export to CONLLU format
# Globals:
#   $udpipe_bin
#   $udpipe_model
# Arguments:
#   None
# Returns:
#   Creates *.conllu files
#######################################
convert_to_conllu()
{
    # List all Elan files
    files=$(find .. -name "*.txt")

    for file in $files
    do
	# Keep the basename, change extension
	output=$(basename -s ".txt" $file).conllu

	echo "Converting $file to $output..."

	# Keep only the speech before running udpipe
	cat $file | grep -E '^[0-9]' | sed -e 's/[0-9]\{2,\}\s\w\{1,\}\s*//g' | $udpipe_bin --tag --parse --output=conllu --input=horizontal $udpipe_model > tmp.conllu
	mv tmp.conllu $output
    done

    echo "Done."
}


check_for_udpipe
convert_to_conllu

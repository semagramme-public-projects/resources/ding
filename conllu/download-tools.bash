#!/usr/bin/bash

TOOLS_DIR=.

# Download UDPipe and languages models
if [ ! -f udpipe ]; then
  wget https://github.com/ufal/udpipe/releases/download/v1.2.0/udpipe-1.2.0-bin.zip
  unzip udpipe-1.2.0-bin.zip && mv udpipe-1.2.0-bin/bin-linux64/udpipe ./udpipe
  rm -rf *.zip udpipe-1.2.0-bin
fi

if [ ! -f french-spoken-ud-2.5-191206.udpipe ]; then
  wget https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/french-spoken-ud-2.5-191206.udpipe
fi

cd -
